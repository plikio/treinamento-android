package br.unifor.ilikemyheadphone;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by labm4 on 10/07/2015.
 */
public class ListAdapter extends BaseAdapter {

    LayoutInflater inflater;
    List<Comments> commentsList;

    public ListAdapter (Activity context, List<Comments> commentsList) {
        super();
        this.commentsList = commentsList;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return commentsList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.commentsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Comments comment = this.commentsList.get(position);

        View vi=convertView;

        if (convertView==null) {
            //vi = inflater.inflate(R.layout.list_item, null);

            ViewHolder viewHolder;
            if (convertView==null) {
                convertView = inflater.inflate(R.layout.list_item, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.getPhoto().setImageResource(comment.getPhoto());
            viewHolder.getMain().setText(comment.getComment());
            viewHolder.getGroup().setText(comment.getBrand());
            viewHolder.getType().setText(comment.getModel());

            /*((ImageView) vi.findViewById(R.id.imageViewUser).setImageResource(comment.getPhoto()));
            ((TextView) vi.findViewById(R.id.textViewBrand).setText(comment.getBrand()));
            ((TextView) vi.findViewById(R.id.textViewModel).setText(comment.getModel()));
            ((TextView) vi.findViewById(R.id.textViewComment).setText(comment.getComment()));
            */
        }


        return null;
    }
}
