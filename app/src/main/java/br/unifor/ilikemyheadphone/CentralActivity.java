package br.unifor.ilikemyheadphone;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class CentralActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_central);

        ListAdapter adapter = new ListAdapter(this, getComments());
        ListView listView = (ListView) findViewById(R.id.listViewCentral);

        listView.setAdapter(adapter);
    }

    private List<Comments> getComments(){
        List<Comments> comentarios = new ArrayList<>();

        comentarios.add(new Comments("Eu gosto... é muito adulto", "Peppa", "666", R.drawable.profile_icon));

        return comentarios;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_central, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.action_logout) {
            return true;
        }
        else if(id == R.id.action_new_comment){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
