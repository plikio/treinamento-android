package br.unifor.ilikemyheadphone;

/**
 * Created by labm4 on 10/07/2015.
 */
public class Comments {

    private String comment, brand, model;
    private int photo;

    public Comments(String comment, String brand, String model, int photo) {
        this.comment = comment;
        this.brand = brand;
        this.model = model;
        this.photo = photo;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


}
