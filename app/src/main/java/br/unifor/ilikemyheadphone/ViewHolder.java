package br.unifor.ilikemyheadphone;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by labm4 on 10/07/2015.
 */
public class ViewHolder {

    private TextView main, group, type;
    private ImageView photo;

    public TextView getMain() {
        return main;
    }

    public TextView getGroup() {
        return group;
    }

    public TextView getType() {
        return type;
    }

    public ImageView getPhoto() {
        return photo;
    }

    public ViewHolder(View v) {

        this.photo = (ImageView) v.findViewById(R.id.imageViewUser);
        this.main = (TextView) v.findViewById(R.id.textViewComment);
        this.group = (TextView) v.findViewById(R.id.textViewBrand);
        this.type = (TextView) v.findViewById(R.id.textViewModel);
    }



}
