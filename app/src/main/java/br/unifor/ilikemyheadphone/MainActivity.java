package br.unifor.ilikemyheadphone;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    private EditText editTextEmail, editTextPassword;
    private Button buttonLogIn;
    private ImageView imageViewLogo;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        buttonLogIn = (Button) findViewById(R.id.buttonLogIn);
        imageViewLogo = (ImageView) findViewById(R.id.imageViewLogo);


    }



    public void forgotPassword (View v) {

        Intent intent = new Intent(this, ForgotActivity.class);
        startActivity(intent);

    }

    public void register (View v) {

        Intent intent = new Intent(this, ActivityRegister.class);
        startActivity(intent);

    }

    public void logIn (View v) {

        Intent intent = new Intent(this, CentralActivity.class);
        startActivity(intent);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.action_logout) {
            return true;
        }
        else if(id == R.id.action_new_comment){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
