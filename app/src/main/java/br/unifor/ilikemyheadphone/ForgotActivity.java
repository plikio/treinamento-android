package br.unifor.ilikemyheadphone;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;


public class ForgotActivity extends ActionBarActivity {

    private EditText editTextEmail;
    private Button buttonRecoverPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        editTextEmail = (EditText) findViewById(R.id.editTextEmailRecover);
        buttonRecoverPassword = (Button) findViewById(R.id.buttonRecover);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_forgot, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.action_logout) {
            return true;
        }
        else if(id == R.id.action_new_comment){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
